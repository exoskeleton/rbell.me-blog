---
title: "About"
date: 2021-11-13T11:45:43Z
draft: false
---

DevOps Engineer with a keen interest in all things cloud, infrastructure as code, and CI/CD.

I also like podcasts, watches, speculative fiction, and running around with my doberman.
