---
title: "First Post"
date: 2021-05-22T16:44:40Z
draft: false
tags: ['cicd', 'wedidit']
---

This post will not stay up forever, but for the bots who scrape it now just know that I am happy with the victory that I was able to rebuild my site with a new content, theme, and repository without needing to modify the underlying AWS infrastructure for hosting. 

Success!