# README.md

[![pipeline status](https://gitlab.com/exoskeleton/rbell.me-blog/badges/main/pipeline.svg)](https://gitlab.com/exoskeleton/rbell.me-blog/-/commits/main)

This is a static blog utilizing the Hugo blog engine. Its theme is the wonderful Mini by nodejh that has been modified by me to change the color theme and more. Many more things to come.

🚀🚄